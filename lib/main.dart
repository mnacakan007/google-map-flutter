import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(title: const Text('Google Maps')),
      body: MapsDemo(),
    ),
  ));
}

class MapsDemo extends StatefulWidget {
  @override
  State createState() => MapsDemoState();
}

class MapsDemoState extends State<MapsDemo> {
  Map<String, double> currentLocation = new Map();
  StreamSubscription<Map<String, double>> locationSubscription;

  Location location = new Location();
  String error;

  final double _initFabHeight = 120.0;
  double _fabHeight;
  double _panelHeightOpen = 575.0;
  double _panelHeightClosed = 55.0;
  double lat = 0.0;
  double lng = 0.0;

  bool currentWidget = true;

  Completer<GoogleMapController> _controller = Completer();
  static final CameraPosition _initialCamera = CameraPosition(
    target: LatLng(0, 0),
    zoom: 4,
  );

  CameraPosition _currentCameraPosition;
  GoogleMap googleMap;
  GoogleMapController mapController;
  dynamic _currentMapType;


  @override
  void initState() {
    super.initState();

    currentLocation['latitude'] = 0;
    currentLocation['longitude'] = 0;
    initPlatformState();
    locationSubscription =
        location.onLocationChanged().listen((Map<String, double> result) {
          setState(() {
            currentLocation = result;
            lat = currentLocation['latitude'];
            lng = currentLocation['longitude'];
            print("currentLocation: $currentLocation");
          });
        });

    print("currentLocation: $currentLocation");
    _fabHeight = _initFabHeight;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[

          SlidingUpPanel(
            maxHeight: _panelHeightOpen,
            minHeight: _panelHeightClosed,
            parallaxEnabled: true,
            parallaxOffset: .5,
            body: _body(),
            panel: _panel(),
            borderRadius: BorderRadius.only(topLeft: Radius.circular(18.0),
                topRight: Radius.circular(18.0)),
            onPanelSlide: (double pos) =>
                setState(() {
                  _fabHeight = pos * (_panelHeightOpen - _panelHeightClosed) +
                      _initFabHeight;
                }),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 8.0, 5.0, 10.0),
            child: Align(
              alignment: Alignment.topLeft,
              child: Column(
                children: <Widget>[
                  FloatingActionButton(
                    onPressed: _onMapTypeButtonPressed,
                    materialTapTargetSize: MaterialTapTargetSize.padded,
                    backgroundColor: Colors.green,
                    child: const Icon(Icons.map, size: 36.0),
                  ),
                  SizedBox(height: 16.0),
                  FloatingActionButton(
                    onPressed: _onAddMarkerButtonPressed,
                    materialTapTargetSize: MaterialTapTargetSize.padded,
                    backgroundColor: Colors.green,
                    child: const Icon(Icons.add_location, size: 36.0),
                  ),
                  SizedBox(height: 16.0),
                  FloatingActionButton(
                    mini: true,
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) =>
                              locationDialog(mapController.cameraPosition));
                    },
                    materialTapTargetSize: MaterialTapTargetSize.padded,
                    backgroundColor: Colors.white10,
                    child: const Icon(Icons.my_location, size: 36.0),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 8.0, 5.0, 10.0),
            child: Align(
              alignment: Alignment.topRight,
              child: Column(
                children: <Widget>[
                  FloatingActionButton(
                    child: Icon(
                      Icons.gps_fixed,
                      color: Theme
                          .of(context)
                          .primaryColor,
                    ),
                    onPressed: () {
                      mapController.animateCamera(
                          CameraUpdate.newCameraPosition(CameraPosition(
                              target: LatLng(currentLocation['latitude'],
                                  currentLocation['longitude']),
                              // bearing: 90.0
                              zoom: 15.0,
                              tilt: 30.0)));
                    },
                    backgroundColor: Colors.white,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 5.0,
            child: Container(
              padding: const EdgeInsets.all(5.0),
              child: RaisedButton(
                child: Text('Move Camera'),
                color: Colors.white,
                onPressed: () {
                  mapController.animateCamera(
                    // CameraUpdate.newLatLng(LatLng(32.7266, 74.8570)),
                      CameraUpdate.newCameraPosition(CameraPosition(
                          target: LatLng(40.139755, 44.485146),
                          // bearing: 90.0
                          zoom: 15.0,
                          tilt: 30.0)));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onMapTypeButtonPressed() {
    setState(() {
      mapController.updateMapOptions(
          GoogleMapOptions(mapType: _currentMapType));
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  void initPlatformState() async {
    Map<String, double> my_location;
    try {
      my_location = (await location.getLocation()) as Map<String, double>;
      error = "";
    } on PlatformException catch (c) {
      if (c.code == "PERMISSION_DENIED")
        error = "Perpission Denied";
      else if (c.code == "PERMISSION_DENIED_NEVER_ASK")
        error =
        "Permission denied - please ask the user to enable it from the app settings";
      my_location = null;
    }

    print(currentLocation);
    setState(() {
      currentLocation = my_location;
    });
  }

  void _onAddMarkerButtonPressed() {
    mapController.addMarker(
      MarkerOptions(
        position: LatLng(
          mapController.cameraPosition.target.latitude,
          mapController.cameraPosition.target.longitude,
        ),
        infoWindowText: InfoWindowText('My Location', 'Home'),
        icon: BitmapDescriptor.defaultMarker,
      ),
    );
  }

  Widget locationDialog(CameraPosition pos) {
    return CupertinoAlertDialog(
      title: new Text("Current Location"),
      content: Column(
        children: [
          Text('latitude : ' + pos.target.latitude.toDouble().toStringAsFixed(6)),
          Text('longitude : ' + pos.target.longitude.toDouble().toStringAsFixed(6))
        ],
      ),
      actions: <Widget>[
        new CupertinoDialogAction(
            child: const Text('Discard'),
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pop(context, 'Discard');
            }),
        new CupertinoDialogAction(
            child: const Text('Cancel'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context, 'Cancel');
            }),
      ],
    );
  }

  Widget _panel() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 12.0,),

        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 30,
              height: 5,
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.all(Radius.circular(12.0))
              ),
            ),
          ],
        ),

        SizedBox(height: 8.0,),

        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Explore Yerevan",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 18.0,
              ),
            ),
          ],
        ),

        SizedBox(height: 36.0,),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _button("Popular", Icons.favorite, Colors.blue),
            _button("Food", Icons.restaurant, Colors.red),
            _button("Events", Icons.event, Colors.amber),
            _button("More", Icons.more_horiz, Colors.green),
          ],
        ),

        SizedBox(height: 36.0,),

        Container(
          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Images", style: TextStyle(fontWeight: FontWeight.w600,)),

              SizedBox(height: 12.0,),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                  CachedNetworkImage(
                    imageUrl: "http://www.oryxitourism.com/wp-content/uploads/2016/08/Armenia-Tour.jpg",
                    height: 120.0,
                    width: (MediaQuery
                        .of(context)
                        .size
                        .width - 48) / 2 - 2,
                    fit: BoxFit.cover,
                  ),

                  CachedNetworkImage(
                    imageUrl: "https://images.squarespace-cdn.com/content/v1/57b9b98a29687f1ef5c622df/1472654514146-SSSPYPQAVO1F9URTI04N/ke17ZwdGBToddI8pDm48kBbOjajeQQtePfd1O4jqnaAUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcXMwU3bcPXQlGfZeAHgJ5LFKd7rsAsrvG_OCzJm9yN6vMX6yVKuxxP6-raXwpph8G/republic+square+yerevan?format=1500w",
                    width: (MediaQuery
                        .of(context)
                        .size
                        .width - 48) / 2 - 2,
                    height: 120.0,
                    fit: BoxFit.cover,
                  ),

                ],
              ),
            ],
          ),
        ),

        SizedBox(height: 36.0,),

        Container(
          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                  "About",
                  style: TextStyle(fontWeight: FontWeight.w600,)),

              SizedBox(height: 12.0,),

              Text(
                "Yerevan (/jɛrəˈvɑːn/ YERR-ə-VAHN; Armenian: Երևան[a] [jɛɾɛˈvɑn] (About this soundlisten),"
                    " sometimes spelled Erevan)[b] is the capital and largest city of Armenia as well as one of"
                    " the world's oldest continuously inhabited cities.[17] Situated along the Hrazdan River,"
                    " Yerevan is the administrative, cultural, and industrial center of the country."
                    " It has been the capital since 1918, the fourteenth in the history of Armenia and "
                    "the seventh located in or around the Ararat plain. The city also serves as the seat "
                    "of the Araratian Pontifical Diocese; the largest diocese of the Armenian Apostolic "
                    "Church and one of the oldest dioceses in the world.[18]"
                    "The history of Yerevan dates back to the 8th century BC, with"
                    " the founding of the fortress of Erebuni in 782 BC by king "
                    "Argishti I at the western extreme of the Ararat plain.[19] Erebuni was "
                    "designed as a great administrative and religious centre, a fully royal capital."
                    "[20] By the late ancient Armenian Kingdom, new capital cities were established and "
                    "Yerevan declined in importance. Under Iranian and Russian rule, it was the center of "
                    "the Erivan Khanate from 1736 to 1828 and the Erivan Governorate from 1850 to 1917,"
                    "respectively. After World War I, Yerevan became the capital of the First Republic"
                    "of Armenia as thousands of survivors of the Armenian Genocide in the Ottoman Empire"
                    "arrived in the area.[21] The city expanded rapidly during the 20th century as Armenia"
                    "became part of the Soviet Union. In a few decades, Yerevan was transformed from a provincial"
                    "town within the Russian Empire to Armenia's principal cultural, artistic, and industrial center,"
                    "as well as becoming the seat of national government.",
                maxLines: 14,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ),


      ],
    );
  }

  Widget _button(String label, IconData icon, Color color) {
    return Column(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(16.0),
          child: Icon(
            icon,
            color: Colors.white,
          ),
          decoration: BoxDecoration(
              color: color,
              shape: BoxShape.circle,
              boxShadow: [BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.15),
                blurRadius: 8.0,
              )
              ]
          ),
        ),

        SizedBox(height: 12.0,),

        Text(label),
      ],

    );
  }

  Widget _body() {
    return GoogleMap(
      onMapCreated: (controller) {
        mapController = controller;
      },
      options: GoogleMapOptions(
        scrollGesturesEnabled: true,
        tiltGesturesEnabled: true,
        rotateGesturesEnabled: true,
        myLocationEnabled: true,
        compassEnabled: true,
        mapType: MapType.normal,
        trackCameraPosition: true,
        zoomGesturesEnabled: true,
        cameraPosition: const CameraPosition(
          target: LatLng(40.177863, 44.512604),
          zoom: 11.0,
        ),
      ),
    );
  }

}
